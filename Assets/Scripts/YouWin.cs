﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YouWin : MonoBehaviour {
    public int currentLanguage;
    public AudioSource source;
    public AudioClip youWinFr, youWinEn, youWinEs;
    // Use this for initialization
    void Start () {
        currentLanguage = PlayerPrefs.GetInt("currentLanguage");
        source = GetComponent<AudioSource>();
        switch (currentLanguage)
        {
            case 0:
                source.PlayOneShot(youWinFr, 1.0f);
                break;
            case 1:
                source.PlayOneShot(youWinEn, 1.0f);
                break;
            case 2:
                source.PlayOneShot(youWinEs, 1.0f);
                break;
        }
    }
}
