﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedGif : MonoBehaviour {
    public GameObject SplashScreen, Decouverte, Souris, CreationDeLaSourceDuMal, Propagation, canvas, tutoriel, minimapCam, minimapCanvas, Zombie;
    public AudioSource FondSonore, ici;
    public AudioClip decouverteFr, sourisFr, creationFr, propagationFr, decouverteEn, sourisEn, creationEn, propagationEn, decouverteEs, sourisEs, creationEs, propagationEs;
    public int currentLanguage;
    public bool EnCours;
    void Start ()
    {
        StartCoroutine(WaitMe());
        ici = GetComponent<AudioSource>();
        currentLanguage = PlayerPrefs.GetInt("currentLanguage");
        EnCours = true;
    }
    IEnumerator WaitMe()
    {
        FondSonore.volume = 0.5f;
        canvas.SetActive(false);
        minimapCam.SetActive(false);
        minimapCanvas.SetActive(false);
        tutoriel.SetActive(false);
        Zombie.SetActive(false);
        SplashScreen.SetActive(true);
        Decouverte.SetActive(false);
        Souris.SetActive(false);
        CreationDeLaSourceDuMal.SetActive(false);
        Propagation.SetActive(false);

        yield return new WaitForSeconds(4);

        SplashScreen.SetActive(false);
        Decouverte.SetActive(true);
        Souris.SetActive(false);
        CreationDeLaSourceDuMal.SetActive(false);
        Propagation.SetActive(false);

        switch (currentLanguage)
        {
            case 0:
                ici.PlayOneShot(decouverteFr, 1f);
                break;
            case 1:
                ici.PlayOneShot(decouverteEn, 1f);
                break;
            case 2:
                ici.PlayOneShot(decouverteEs, 1f);
                break;
        }

        yield return new WaitForSeconds(8);

        SplashScreen.SetActive(false);
        Decouverte.SetActive(false);
        Souris.SetActive(true);
        CreationDeLaSourceDuMal.SetActive(false);
        Propagation.SetActive(false);

        switch (currentLanguage)
        {
            case 0:
                ici.PlayOneShot(sourisFr, 1f);
                break;
            case 1:
                ici.PlayOneShot(sourisEn, 1f);
                break;
            case 2:
                ici.PlayOneShot(sourisEs, 1f);
                break;
        }


        yield return new WaitForSeconds(8);

        SplashScreen.SetActive(false);
        Decouverte.SetActive(false);
        Souris.SetActive(false);
        CreationDeLaSourceDuMal.SetActive(true);
        Propagation.SetActive(false);

        switch (currentLanguage)
        {
            case 0:
                ici.PlayOneShot(creationFr, 1f);
                break;
            case 1:
                ici.PlayOneShot(creationEn, 1f);
                break;
            case 2:
                ici.PlayOneShot(creationEs, 1f);
                break;
        }

        yield return new WaitForSeconds(11);

        SplashScreen.SetActive(false);
        Decouverte.SetActive(false);
        Souris.SetActive(false);
        CreationDeLaSourceDuMal.SetActive(false);
        Propagation.SetActive(true);

        switch (currentLanguage)
        {
            case 0:
                ici.PlayOneShot(propagationFr, 1f);
                break;
            case 1:
                ici.PlayOneShot(propagationEn, 1f);
                break;
            case 2:
                ici.PlayOneShot(propagationEs, 1f);
                break;
        }

        yield return new WaitForSeconds(20);

        SplashScreen.SetActive(false);
        Decouverte.SetActive(false);
        Souris.SetActive(false);
        CreationDeLaSourceDuMal.SetActive(false);
        Propagation.SetActive(false);
        canvas.SetActive(true);
        minimapCam.SetActive(true);
        minimapCanvas.SetActive(true);
        tutoriel.SetActive(true);
        FondSonore.volume = 1f;
        EnCours = false;
    }
}
