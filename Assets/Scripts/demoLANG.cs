﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class demoLANG : MonoBehaviour {
    public Text FindGun, Attack, RetourMenu;
    // Use this for initialization
    void Awake() {
        switch (PlayerPrefs.GetInt("currentLanguage"))
        {
            case 0:
                FindGun.text = "Trouver Une Arme";
                Attack.text = "Attaquer Un Zombie\n(!! Ne Pas le Toucher !!)";
                RetourMenu.text = "Pour Retourner\nAu Menu";
                break;
            case 1:
                FindGun.text = "Find a Gun";
                Attack.text = "Attack a Zombie\n(!! Don't Touch It !!)";
                RetourMenu.text = "To Go Back\nTo Menu";
                break;
            case 2:
                FindGun.text = "Encuentre un arma";
                Attack.text = "Ataca a un zombie\n(!!! No lo toques!!!)";
                RetourMenu.text = "Para Volver\nAl Menú";
                break;
        }
    }
}
