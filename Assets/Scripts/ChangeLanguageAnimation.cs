﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeLanguageAnimation : MonoBehaviour {
    public Text Decouverte;
    public Text Souris;
    public Text CreationMal;
    public Text Propagation;
    private string decouverte;
    private string souris;
    private string creationmal;
    private string propagation;
    public GameObject Language;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        decouverte = Language.GetComponent<xmlReader>().decouverte;
        souris = Language.GetComponent<xmlReader>().souris;
        creationmal = Language.GetComponent<xmlReader>().creationmal;
        propagation = Language.GetComponent<xmlReader>().propagation;
        Decouverte.text = decouverte;
        Souris.text = souris;
        CreationMal.text = creationmal;
        Propagation.text = propagation;
    }
}