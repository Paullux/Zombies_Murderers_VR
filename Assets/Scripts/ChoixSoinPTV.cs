﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixSoinPTV : MonoBehaviour {

    private int CoupFeu;
    //liés à la progressbar
    public float progress = 0.0f;
    public Texture2D emptyProgressBar; // Set this in inspector.
    public Texture2D fullProgressBar; // Set this in inspector.
    public Vector2 pos = new Vector2(20, 40);
    public Vector2 size = new Vector2(60, 20);
    public Scrollbar ProgressBarre;
    Menu Menu_Script;

    void Start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
        //Menu_Script.UpdateTXTArmor(15, Menu_Script.HealthNumberCell[15].ToString() + "%");
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf)
        {
            Menu_Script.UpdateTXTHealth(15, Menu_Script.HealthNumberCell[15].ToString() + "%");
        }

        //Liés à la progressbar
        progress = (float)Menu_Script.HealthNumberCell[15] / 100;
        ProgressBarre.size = progress;
    }
}
