﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : MonoBehaviour {

    private bool Armur = false;
    public GameObject joueur;
    public GameObject ElemArmure;
    public int AddPTA, typeA;
    private int pta, newPTA;
    Menu Menu_Script;
    // Update is called once per frame

    void Start()
    {
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }

    void Update()
    {
        if (Armur)
        {
            pta = Menu_Script.ArmorNumberCell[15];

            if (pta < 100)
            {
                newPTA = AddPTA + pta;
                if (newPTA > 100) newPTA = 100;
                Menu_Script.ArmorNumberCell[15] = newPTA;
                Armur = false;
            }
            else
            {
                for (int cell = 0; cell < 15; cell++)
                {
                    if (Menu_Script.ArmorTypeCell[cell] == 0 || Menu_Script.ArmorTypeCell[cell] == typeA)
                    {
                        Menu_Script.ArmorTypeCell[cell] = typeA;
                        if (Menu_Script.ArmorNumberCell[cell] < 5)
                        {
                            Menu_Script.ArmorNumberCell[cell] += 1;
                            Menu_Script.UpdateTXTArmor(cell, Menu_Script.ArmorNumberCell[cell].ToString() + "/5");
                            Menu_Script.UpdateTXTArmor(15, "");
                            Menu_Script.UpdateImageArmor(cell, typeA, true);
                            Armur = false;
                            break;
                        }
                    }
                }
            }
            Destroy(ElemArmure);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun" || other.gameObject.tag == "Player")
        {
            Armur = true;

        }
    }
}
