﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixArmure : MonoBehaviour {

    private int nrSlotArmor, CoupFeu;
    public bool StartBar = false;
    Menu Menu_Script;

    void Start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }
    void OnEnable()
    {
        StartBar = false;
    }

    void Update()
    {
        if (StartBar && Input.GetKeyDown((KeyCode)CoupFeu))
        {
            if (Menu_Script.ArmorNumberCell[nrSlotArmor] > 0)
            {
                Menu_Script.ArmorNumberCell[nrSlotArmor] -= 1;
                if (Menu_Script.ArmorTypeCell[nrSlotArmor] == 1) Menu_Script.ArmorNumberCell[15] += 20;
                if (Menu_Script.ArmorTypeCell[nrSlotArmor] == 2) Menu_Script.ArmorNumberCell[15] += 50;
                if (Menu_Script.ArmorTypeCell[nrSlotArmor] == 3) Menu_Script.ArmorNumberCell[15] += 70;
                if (Menu_Script.ArmorNumberCell[15] > 100) Menu_Script.ArmorNumberCell[15] = 100;
                Menu_Script.UpdateTXTArmor(15, "");
            }
            if (Menu_Script.ArmorTypeCell[nrSlotArmor] != 0) Menu_Script.UpdateTXTArmor(nrSlotArmor, Menu_Script.ArmorNumberCell[nrSlotArmor].ToString() + "/5");
            if (Menu_Script.ArmorNumberCell[nrSlotArmor] == 0)
            {
                Menu_Script.UpdateTXTArmor(nrSlotArmor, "");
                Menu_Script.UpdateImageArmor(nrSlotArmor, Menu_Script.ArmorTypeCell[nrSlotArmor], false);
                Menu_Script.ArmorTypeCell[nrSlotArmor] = 0;
            }
        }
    }
    public void BarreStart()
    {
        StartBar = true;
        nrSlotArmor = transform.GetSiblingIndex();
    }
    public void BarreStop()
    {
        StartBar = false;
    }
}


