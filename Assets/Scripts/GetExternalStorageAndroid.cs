﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
#if UNITY_ANDROID && !UNITY_EDITOR
public class GetExternalStorageAndroid
{
	//notre chaîne de caractères qui contiendra le Path
	public static string externalStorage{ get; protected set; }
	
	static GetExternalStorageAndroid()
	{
		//Récupérer la classe Environment d'Android
		AndroidJavaClass envClass = new AndroidJavaClass("android.os.Environment");
		//Récupérer le dossier traditionnel contenant les images et vidéos
		string folderDCIM = envClass.GetStatic <string> ("DIRECTORY_DCIM");
		//Récupérer le dossier
		AndroidJavaObject externalStorageFile =  envClass.CallStatic <AndroidJavaObject> ("getExternalStoragePublicDirectory", folderDCIM);
		//Récupérer le chemin absolu vers ce dossier
		externalStorage = externalStorageFile.Call <string> ("getAbsolutePath");
	}
}
#endif