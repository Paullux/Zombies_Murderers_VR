﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CaptureEcran : MonoBehaviour
{
    public AudioClip AppPhoto;
    public AudioSource source;
    public bool Shot_Taken;
    private string filename, mypath, pathfile, pathdest, folderDCIM, myfolder;
    private float TempsDepart, timescaling;
    private int TakeShot;

    void Start()
    {
        mypath = "./ZombiesMurderers/";
#if UNITY_ANDROID && !UNITY_EDITOR
        //Récupérer la classe Environment d'Android
        AndroidJavaClass envClass = new AndroidJavaClass("android.os.Environment");
        
        //Récupérer le dossier traditionnel contenant les images et vidéos
        folderDCIM = envClass.GetStatic<string>("DIRECTORY_DCIM");
        
        //Récupérer le dossier
        AndroidJavaObject externalStorageFile = envClass.CallStatic<AndroidJavaObject>("getExternalStoragePublicDirectory", folderDCIM);
        
        //Récupérer le chemin absolu vers ce dossier
        myfolder = externalStorageFile.Call<string>("getAbsolutePath");
        
        //Création de mon dossier Image Perso
        mypath = myfolder + "/ZombiesMurderers/";
#endif
        if (!Directory.Exists(mypath)) Directory.CreateDirectory(mypath);

        Shot_Taken = false;

        TakeShot = PlayerPrefs.GetInt("screenshotInt");
    }

    void Update()
    {
        if (!Shot_Taken) StopCoroutine("PrendreUnePhoto");
        if (!Shot_Taken && Input.GetKeyDown((KeyCode)TakeShot))
        {
            StartCoroutine("PrendreUnePhoto");
        }
    }
    IEnumerator PrendreUnePhoto()
    {
        Shot_Taken = true;
        filename = "Screenshot_" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";


        timescaling = Time.timeScale;
        Time.timeScale = 1;
        AudioSource.PlayClipAtPoint(AppPhoto, Vector3.zero, 1f);
        Time.timeScale = timescaling;

        ScreenCapture.CaptureScreenshot(filename);

        TempsDepart = Time.unscaledTime;
        while ((Time.unscaledTime - TempsDepart) < 2f)
        {
            yield return 0;
        }

        pathfile = Application.persistentDataPath + "/" + filename;
        pathdest = mypath + "/" + filename;

        if (File.Exists(pathfile)) File.Move(pathfile, pathdest);
        TempsDepart = Time.unscaledTime;
        while ((Time.unscaledTime - TempsDepart) < 0.3f)
        {
            yield return 0;
        }
        Shot_Taken = false;

        yield return null;
    }
}