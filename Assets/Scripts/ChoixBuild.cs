﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixBuild : MonoBehaviour {

    public GameObject Blank;
    public GameObject Construc1;
    public GameObject Construc2;
    public GameObject Construc3;
    public Text TauxConstruc;
    public string Cell;
    private int TypeCell, vide, nrSlotGun, CoupFeu;
    public bool StartBar = false;
    Menu Menu_Script;

    void Start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }

    void OnEnable()
    {
        StartBar = false;
    }

    void Update()
    {

        if (StartBar && Input.GetKeyDown((KeyCode)CoupFeu))
        {
            if (Menu_Script.GunNumberCell[nrSlotGun] > 0) Menu_Script.GunNumberCell[nrSlotGun] -= 1;
            StartBar = false;
        }
    }

    public void BarreStart()
    {
        StartBar = true;
        nrSlotGun = transform.GetSiblingIndex();
    }

    public void BarreStop()
    {
        StartBar = false;
    }
}
