﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveHuman : MonoBehaviour {

    public GameObject PlayerHead;
    private Vector3 wantedPosition;
    private Vector3 newWantedPosition;
    private Vector3 wantedRotation;
    public Vector3 direction;
    private bool ZombieTouch;
    public AudioSource Human;
    public AudioClip OkDacfr, OkDacEn, OkDacEs;
    public AudioClip StayHerefr, StayHereEn, StayHereEs;
    public AudioClip FollowMefr, FollowMeEn, FollowMeEs;
    private bool humanwalk;
    public float speed;
    private bool Encours;
    private GameObject human;
    private float Distance;
    public int currentLanguage;
    // Use this for initialization
    void Start () {
        humanwalk = false;
        Encours = false;
        speed = 2;
        currentLanguage = PlayerPrefs.GetInt("currentLanguage");
        Human = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        wantedPosition = PlayerHead.transform.position;
        newWantedPosition = new Vector3(wantedPosition.x, 0f, wantedPosition.z);
        Distance = Vector3.Distance(PlayerHead.transform.position, transform.position);
        if (humanwalk && Distance > 3)
        {
            GetComponent<Animator>().SetTrigger("Walk");
            GetComponentInParent<Rigidbody>().MovePosition(transform.position + direction * 1.5f * speed * Time.deltaTime);
        }
        else
        {
            GetComponent<Animator>().SetTrigger("Wait");
        }
            wantedRotation = newWantedPosition - transform.position;
        direction = wantedRotation.normalized;
        transform.LookAt(newWantedPosition);
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun")
        {
            GetComponentInParent<Rigidbody>().isKinematic = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun")
        {
            GetComponentInParent<Rigidbody>().isKinematic = false;
        }
    }
    public void setFollowMe()
    {
        if (!Encours) StartCoroutine("Reponse");
    }
    IEnumerator Reponse()
    {
        Encours = true;
        if (!humanwalk)
        {
            switch (currentLanguage)
            {
                case 0:
                    Human.PlayOneShot(FollowMefr, 1.0f);
                    yield return new WaitForSeconds(1f);
                    Human.PlayOneShot(OkDacfr, 0.7f);
                    yield return new WaitForSeconds(.6f);
                    break;
                case 1:
                    Human.PlayOneShot(FollowMeEn, 1.0f);
                    yield return new WaitForSeconds(1f);
                    Human.PlayOneShot(OkDacEn, 0.7f);
                    yield return new WaitForSeconds(.6f);
                    break;
                case 2:
                    Human.PlayOneShot(FollowMeEs, 1.0f);
                    yield return new WaitForSeconds(1f);
                    Human.PlayOneShot(OkDacEs, 0.7f);
                    yield return new WaitForSeconds(.6f);
                    break;
            }
        }
        else
        {
            switch (currentLanguage)
            {
                case 0:
                    Human.PlayOneShot(StayHerefr, 1.0f);
                    yield return new WaitForSeconds(1f);
                    Human.PlayOneShot(OkDacfr, 0.7f);
                    yield return new WaitForSeconds(.6f);
                    break;
                case 1:
                    Human.PlayOneShot(StayHereEn, 1.0f);
                    yield return new WaitForSeconds(1f);
                    Human.PlayOneShot(OkDacEn, 0.7f);
                    yield return new WaitForSeconds(.6f);
                    break;
                case 2:
                    Human.PlayOneShot(StayHereEs, 1.0f);
                    yield return new WaitForSeconds(1.5f);
                    Human.PlayOneShot(OkDacEs, .7f);
                    yield return new WaitForSeconds(.6f);
                    break;
            }
        }
        humanwalk = !humanwalk;
        Encours = false;
    }

}
