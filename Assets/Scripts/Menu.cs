﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    private GameObject Inventaire01, Inventaire02, Inventaire03;
    public GameObject Anim;
    [SerializeField] Transform target;
    public Text TextInventaire01;
    public Text TextInventaire02;
    public Text TextInventaire03;
    private string attack;
    private string armor;
    private string health;
    public GameObject Language;
    private int AfficheMenu;
    public int[] HealthTypeCell;
    public int[] HealthNumberCell;
    public int[] ArmorTypeCell;
    public int[] ArmorNumberCell;
    public int[] GunTypeCell;
    public int[] GunNumberCell;
    public bool paused = false;
    private bool AnimEncours = false;
    private bool TutoBool = false;
    private bool ZombBool = false;
    // Use this for initialization
    void Start()
    {
        GetComponent<Canvas>().enabled = false;
        
        Time.timeScale = 1;
        attack = Language.GetComponent<xmlReader>().attack;
        armor = Language.GetComponent<xmlReader>().armor;
        health = Language.GetComponent<xmlReader>().health;
        TextInventaire01.text = attack;
        TextInventaire02.text = armor;
        TextInventaire03.text = health;
        PlayerPrefs.SetInt("MenuActive", 0);
        AfficheMenu = PlayerPrefs.GetInt("menuInt");
        Inventaire01 = transform.GetChild (0).gameObject;
        Inventaire02 = transform.GetChild (2).gameObject;
        Inventaire03 = transform.GetChild (4).gameObject;
        HealthTypeCell = new int[16];
        HealthNumberCell = new int[16];
        ArmorTypeCell = new int[16];
        ArmorNumberCell = new int[16];
        GunTypeCell = new int[16];
        GunNumberCell = new int[16];
        HealthNumberCell[15] = 100;
        if (PlayerPrefsX.GetIntArray("HealthType").Length == 16) HealthTypeCell = PlayerPrefsX.GetIntArray("HealthType");
        if (PlayerPrefsX.GetIntArray("HealthNumber").Length == 16) HealthNumberCell = PlayerPrefsX.GetIntArray("HealthNumber");
        if (PlayerPrefsX.GetIntArray("ArmorType").Length == 16) ArmorTypeCell = PlayerPrefsX.GetIntArray("ArmorType");
        if (PlayerPrefsX.GetIntArray("ArmorNumber").Length == 16) ArmorNumberCell = PlayerPrefsX.GetIntArray("ArmorNumber");
        if (PlayerPrefsX.GetIntArray("GunType").Length == 16) GunTypeCell = PlayerPrefsX.GetIntArray("GunType");
        if (PlayerPrefsX.GetIntArray("GunNumber").Length == 16) GunNumberCell = PlayerPrefsX.GetIntArray("GunNumber");
        if (HealthNumberCell[15] <= 0) HealthNumberCell[15] = 100;

        for (int cell=0; cell <= 15; cell++)
        {
            UpdateTXTGun(cell, GunNumberCell[cell].ToString() + "/5");
            UpdateTXTHealth(cell, HealthNumberCell[cell].ToString() + "/5");
            UpdateTXTArmor(cell, ArmorNumberCell[cell].ToString() + "/5");

            if (GunNumberCell[cell] == 0)
            {
                UpdateTXTGun(cell, "");
            }
            if (HealthNumberCell[cell] == 0)
            {
                UpdateTXTHealth(cell, "");
            }
            if (ArmorNumberCell[cell] == 0)
            {
                UpdateTXTArmor(cell, "");
            }
            if (cell < 14)
            {
                UpdateImageGun(cell, GunTypeCell[cell], true);
                UpdateImageHealth(cell, HealthTypeCell[cell], true);
                UpdateImageArmor(cell, ArmorTypeCell[cell], true);
            }
            
            if (cell == 14)
            {
                UpdateImageHealth(cell, HealthTypeCell[cell], true);
                UpdateImageArmor(cell, ArmorTypeCell[cell], true);
                int InChargeur = GunNumberCell[14];
                int Balles = GunTypeCell[14];
                string EnCharge = InChargeur.ToString() + "/" + Balles.ToString();
                UpdateTXTGun(cell, EnCharge);
            }
            if (cell == 15)
            {
                UpdateTXTGun(cell, GunNumberCell[cell].ToString());
                UpdateTXTHealth(cell, HealthNumberCell[cell].ToString() + "%");
                UpdateTXTArmor(cell, ArmorNumberCell[cell].ToString() + "%");
            }
            Time.timeScale = 1;
        }



    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown((KeyCode)AfficheMenu))
        {
            paused = !paused;
        }

        if (Anim.GetComponentInChildren<AnimatedGif>() != null)
        {
            AnimEncours = Anim.GetComponentInChildren<AnimatedGif>().EnCours;
        }
        else
        {
            AnimEncours = false;
        }
        if (!AnimEncours)
        {
            if (paused)
            {
                PlayerPrefs.SetInt("MenuActive", 1);
                Time.timeScale = 0f;
            }
            else
            {
                Time.timeScale = 1f;
                PlayerPrefs.SetInt("MenuActive", 0);
            }
        }

        GetComponent<Canvas>().enabled = (Time.timeScale == 0);
        
        if (HealthNumberCell[15] < 1)
        {
            GameObject.Find("Tutoriel").GetComponent<Tutorial>().SetTuto(4);
        }
    }

    void LateUpdate ()
    {
        if (Time.timeScale == 1)
        {
            transform.position = new Vector3(target.position.x, transform.position.y, target.position.z);
            float angle = target.transform.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }
    
    public void UpdateTXTGun (int nrSlot, string txt)
    {
        if (nrSlot < 14)
        {
            Inventaire01.transform.GetChild(nrSlot).GetChild(4).GetComponent<Text>().text = txt;
        }
        else
        {
            if (nrSlot == 14)
            {
                Inventaire01.transform.GetChild(nrSlot).GetChild(1).GetComponent<Text>().text = GunNumberCell[nrSlot].ToString() + "/" + GunTypeCell[nrSlot].ToString();
                Image ImageGun = Inventaire01.transform.GetChild(nrSlot).GetComponent<Image>();
                float PerCentGun = 0.8f * (float)(GunNumberCell[nrSlot] + GunTypeCell[nrSlot]) / 1000;
                if (PerCentGun > 0.8f) PerCentGun = 0.8f;
                float REDperCent = 0.8f - 4f * PerCentGun;
                if (REDperCent < 0f) REDperCent = 0f;
                ImageGun.color = new Color(REDperCent, PerCentGun, 0f, 0.8f);
            }
            else
            {
                Inventaire01.transform.GetChild(nrSlot).GetChild(1).GetComponent<Text>().text = GunNumberCell[nrSlot].ToString();
                Image ImageGun = Inventaire01.transform.GetChild(nrSlot).GetComponent<Image>();
                float PerCentGun = 0.8f * (float)(GunNumberCell[nrSlot]) / 500;
                if (PerCentGun > 0.8f) PerCentGun = 0.8f;
                float REDperCent = 0.8f - 4f * PerCentGun;
                if (REDperCent < 0f) REDperCent = 0f;
                ImageGun.color = new Color(REDperCent, PerCentGun, 0f, 0.8f);
            }

        }
    }
    public void UpdateTXTHealth(int nrSlot, string txt)
    {
        if (nrSlot < 15) {

            Inventaire03.transform.GetChild(nrSlot).GetChild(4).GetComponent<Text>().text = txt;
        }
        else
        {
            Inventaire03.transform.GetChild(nrSlot).GetChild(1).GetComponent<Text>().text = HealthNumberCell[nrSlot].ToString() + "%";
            Image ImageHealth = Inventaire03.transform.GetChild(nrSlot).GetComponent<Image>();
            float PerCentHealth = 0.8f * (float)(HealthNumberCell[nrSlot]) / 100;
            if (PerCentHealth > 0.8f) PerCentHealth = 0.8f;
            ImageHealth.color = new Color(0.8f - PerCentHealth, PerCentHealth, 0f, 0.8f);
        }
    }
    public void UpdateTXTArmor(int nrSlot, string txt)
    {
        if (nrSlot < 15)
        {
            Inventaire02.transform.GetChild(nrSlot).GetChild(4).GetComponent<Text>().text = txt;
        }
        else
        {
            Inventaire02.transform.GetChild(nrSlot).GetChild(1).GetComponent<Text>().text = ArmorNumberCell[nrSlot].ToString() + "%";
            Image ImageArmor = Inventaire02.transform.GetChild(nrSlot).GetComponent<Image>();
            float PerCentArmor = 0.8f * (float)(ArmorNumberCell[nrSlot]) / 100;
            if (PerCentArmor > 0.8f) PerCentArmor = 0.8f;
            ImageArmor.color = new Color(0.8f - PerCentArmor, PerCentArmor, 0f, 0.8f);
        }
    }
    public void UpdateImageGun(int nrSlot, int Type, bool Add)
    {
        Inventaire01.transform.GetChild(nrSlot).GetChild(Type).gameObject.SetActive(Add);
    }
    public void UpdateImageHealth(int nrSlot, int Type, bool Add)
    {
        Inventaire03.transform.GetChild(nrSlot).GetChild(Type).gameObject.SetActive(Add);
    }
    public void UpdateImageArmor(int nrSlot, int Type, bool Add)
    {
        Inventaire02.transform.GetChild(nrSlot).GetChild(Type).gameObject.SetActive(Add);
    }
}