﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixArme : MonoBehaviour {

    private int CoupFeu, nrSlotGun;
    public bool StartBar = false;
    Menu Menu_Script;


    void Start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }
    void OnEnable()
    {
        StartBar = false;
    }

    void Update()
    {
        if (StartBar && Input.GetKeyDown((KeyCode)CoupFeu))
        {
            if (Menu_Script.GunNumberCell[nrSlotGun] > 0)
            {
                Menu_Script.GunNumberCell[nrSlotGun] -= 1;
                if (Menu_Script.GunTypeCell[nrSlotGun] == 2) Menu_Script.GunTypeCell[14] += 100;
                if (Menu_Script.GunTypeCell[nrSlotGun] == 3) Menu_Script.GunNumberCell[15] += 15;
                Menu_Script.UpdateTXTGun(14, "");
                Menu_Script.UpdateTXTGun(15, "");
            }
            if (Menu_Script.GunTypeCell[nrSlotGun] != 0) Menu_Script.UpdateTXTGun(nrSlotGun, Menu_Script.GunNumberCell[nrSlotGun].ToString() + "/5");
            if (Menu_Script.GunNumberCell[nrSlotGun]==0)
            {
                Menu_Script.UpdateTXTGun(nrSlotGun, "");
                Menu_Script.UpdateImageGun(nrSlotGun, Menu_Script.GunTypeCell[nrSlotGun], false);
                Menu_Script.GunTypeCell[nrSlotGun] = 0;
            }
        }
    }

    public void BarreStart()
    {
        StartBar = true;
        nrSlotGun = transform.GetSiblingIndex();
    }

    public void BarreStop()
    {
        StartBar = false;
    }

}

