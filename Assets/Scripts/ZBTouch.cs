﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZBTouch : MonoBehaviour
{
    private bool ZombieTouch;
    // Use this for initialization
    public bool getZombieTouch()
    {
        return ZombieTouch;
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun")
        {
            ZombieTouch = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun")
        {
            ZombieTouch = false;
        }
    }
}
