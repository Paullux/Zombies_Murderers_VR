﻿using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveZombie : MonoBehaviour
{
    public GameObject PlayerHead;
    private Vector3 wantedPosition;
    private Vector3 newWantedPosition;
    private Vector3 wantedRotation;
    public float speed;
    public int PointOfLife;
    public GameObject ZombieCible;
    private bool ZombieDead = false;
    public bool ZombieTouch = false;
    public GameObject YoureDead;
    public GameObject Fusil;
    public GameObject Gun;
    public GameObject Couteau;
    public AudioClip Blessure;
    public AudioClip Marche;
    private AudioSource source;
    public GameObject ZombieEnCours;
    public GameObject Tutoriel;
    public int PV, pta;
    public int ZZ;
    public int ZPV;

    //Pour empêcher de continuer d'avancer contre un objet.
    private bool ReturnPos;
    private bool HeadShot;
    Menu Menu_Script;

    void Start()
    {
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
        YoureDead.SetActive(false);
        source = GetComponent<AudioSource>();
        StartCoroutine("ZombieWalk");
        source.loop = true;
        source.clip = Marche;
        source.Play();
    }

    void FixedUpdate()
    {
        

        wantedPosition = PlayerHead.transform.position;
        newWantedPosition = new Vector3(wantedPosition.x, 0f, wantedPosition.z);

        wantedRotation = newWantedPosition - transform.position;
        Vector3 direction = wantedRotation.normalized;

        ZPV = ZombieCible.GetComponent<PointDeVie>().getHP();
        if (ZPV > 0)
        {
            if (!ZombieDead)
            {
                transform.LookAt(newWantedPosition);
                if (ZPV > 20)
                {
                    if (!ReturnPos)
                    {
                        GetComponentInParent<Rigidbody>().MovePosition(transform.position + direction * 1.5f * speed * Time.deltaTime);
                    }
                }
                else
                {
                    ZombieEnCours.GetComponent<Animator>().SetTrigger("Faible");
                }
            }
            if (ZombieDead)
            {
                StartCoroutine("ZombieAfterDead");
            }

            ZombieTouch= GetComponentInParent<ZBTouch>().getZombieTouch();

            if (ZombieTouch)
            {
                PlayerHead.GetComponent<AMMO>().setZombieTouchy(ZombieTouch);
                
                if (ZPV > 0)
                {
                    StopCoroutine("ZombieWalk");
                    source.PlayOneShot(Blessure, 1.0f);
                    ZombieEnCours.GetComponent<Animator>().ResetTrigger("attackOff");
                    ZombieEnCours.GetComponent<Animator>().SetTrigger("attackOn");
                    
                    int PVRandom = UnityEngine.Random.Range(1, 1);
                    if (Menu_Script.ArmorNumberCell[15] >= PVRandom)
                    {
                        Menu_Script.ArmorNumberCell[15] -= PVRandom;
                    }
                    else
                    {
                        Menu_Script.HealthNumberCell[15] -= PVRandom;
                    }
                    source.PlayOneShot(Blessure, 1.0f);
                    Menu_Script.UpdateTXTArmor(15, "");
                    Menu_Script.UpdateTXTHealth(15, "");
                }
                
                if (Menu_Script.HealthNumberCell[15] < 1)
                {
                    YoureDead.SetActive(true);
                    StopCoroutine("ZombieAttack");
                    ZombieEnCours.GetComponent<Animator>().SetTrigger("HumanDead");
                    //GameObject.Find("MortImmante").SetActive(false);
                    Fusil.SetActive(false);
                    Gun.SetActive(false);
                    Couteau.SetActive(false);
                    Tutoriel.GetComponent<Tutorial>().SetTuto(4);
                }
            }
            else
            {
                StopCoroutine("ZombieAttack");
                StopCoroutine("SoundBlessure");
                //StartCoroutine("ZombieWalk");
                ZombieEnCours.GetComponent<Animator>().ResetTrigger("Walk");
                ZombieEnCours.GetComponent<Animator>().SetTrigger("Walk");
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun")
        {
            GetComponentInParent<Rigidbody>().isKinematic = true;
        }
    }
    void OnTriggerStay(Collider other)
    {
        ReturnPos = true;
    }
    void OnTriggerExit(Collider other)
    {
        GetComponentInParent<Rigidbody>().isKinematic = false;
        StopCoroutine("ZombieAttack");
        ZombieEnCours.GetComponent<Animator>().SetTrigger("attackOff");
        ZombieEnCours.GetComponent<Animator>().ResetTrigger("Walk");
        ZombieEnCours.GetComponent<Animator>().SetTrigger("Walk");
    }

    public void setZdead(bool DEAD)
    {
        ZombieDead = DEAD;
        source.clip = Marche;
        source.loop = false;
        source.Stop();
    }
    IEnumerator SoundBlessure()
    {
        while (ZombieTouch)
        {
            yield return new WaitForSeconds(1.025f);
            source.PlayOneShot(Blessure, 1.0f);
        }
    }
    IEnumerator ZombieWalk()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.5f);
            ZombieEnCours.GetComponent<Animator>().ResetTrigger("Walk");
            ZombieEnCours.GetComponent<Animator>().SetTrigger("Walk");
        }
    }
    IEnumerator ZombieAttack()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            ZombieEnCours.GetComponent<Animator>().ResetTrigger("attackOff");
            ZombieEnCours.GetComponent<Animator>().SetTrigger("attackOn");
        }
    }
    IEnumerator HumanDead()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.75f);
            ZombieEnCours.GetComponent<Animator>().SetTrigger("HumanDead");
        }
    }
    IEnumerator ZombieAfterDead()
    {
        yield return new WaitForSeconds(4.0f);
        int childs = transform.childCount;
        for (int i = childs - 1; i > 0; i--)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }
}