﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoixSoin : MonoBehaviour {

    private int nrSlotHealth, CoupFeu;
    public bool StartBar = false;
    Menu Menu_Script;

    void Start()
    {
        CoupFeu = PlayerPrefs.GetInt("FireInt");
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }
    void OnEnable()
    {
        StartBar = false;
    }

    void Update()
    {
        if (StartBar && Input.GetKeyDown((KeyCode)CoupFeu))
        {
            if (Menu_Script.HealthNumberCell[nrSlotHealth] > 0)
            {
                Menu_Script.HealthNumberCell[nrSlotHealth] -= 1;
                if (Menu_Script.HealthTypeCell[nrSlotHealth] == 1) Menu_Script.HealthNumberCell[15] += 20;
                if (Menu_Script.HealthTypeCell[nrSlotHealth] == 2) Menu_Script.HealthNumberCell[15] += 50;
                if (Menu_Script.HealthTypeCell[nrSlotHealth] == 3) Menu_Script.HealthNumberCell[15] += 70;
                if (Menu_Script.HealthNumberCell[15] > 100) Menu_Script.HealthNumberCell[15] = 100;
                Menu_Script.UpdateTXTHealth(15, "");
            }
            if (Menu_Script.HealthTypeCell[nrSlotHealth] != 0) Menu_Script.UpdateTXTHealth(nrSlotHealth, Menu_Script.HealthNumberCell[nrSlotHealth].ToString() + "/5");
            if (Menu_Script.HealthNumberCell[nrSlotHealth] == 0)
            {
                Menu_Script.UpdateTXTHealth(nrSlotHealth, "");
                Menu_Script.UpdateImageHealth(nrSlotHealth, Menu_Script.HealthTypeCell[nrSlotHealth], false);
                Menu_Script.HealthTypeCell[nrSlotHealth] = 0;
            }
        }
    }
    public void BarreStart()
    {
        StartBar = true;
        nrSlotHealth = transform.GetSiblingIndex();
    }
    public void BarreStop()
    {
        StartBar = false;
    }

}