﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUN : MonoBehaviour {
    public GameObject CardbMain;
    public bool AmmoPlus;
    public bool BulletPlus;
    public GameObject GunSurTerrain;
    public GameObject Tutoriel;
    private bool ArmeActive = false;
    Menu Menu_Script;
    // Use this for initialization

    void Start ()
    {
        Menu_Script = GameObject.Find("CanvasMenu").GetComponent<Menu>();
    }
	
	// Update is called once per frame
	void Update () {
        if (AmmoPlus)
        {
            int AmmoRest = Menu_Script.GunTypeCell[14];
            int InChargeur = Menu_Script.GunNumberCell[14];
            if (!ArmeActive)
            {
                CardbMain.GetComponent<AMMO>().setFirstgun(true);
                Menu_Script.GunNumberCell[14] = 15;
                ArmeActive = true;
            }
            Tutoriel.GetComponent<Tutorial>().SetTuto(3);
            Menu_Script.GunTypeCell[14] += 100;
            Destroy(GunSurTerrain);
            for (int cell = 0; cell < 14; cell++)
            {
                if (Menu_Script.GunTypeCell[cell] == 0 || Menu_Script.GunTypeCell[cell] == 2)
                {
                    Menu_Script.GunTypeCell[cell] = 2;
                    if (Menu_Script.GunNumberCell[cell] < 5)
                    {
                        Menu_Script.GunNumberCell[cell] += 1;
                        Menu_Script.UpdateTXTGun(cell, Menu_Script.GunNumberCell[cell].ToString() + "/5");
                        Menu_Script.UpdateImageGun(cell, 2, true);
                        break;
                    }
                }
            }
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "ForLife" || other.gameObject.name == "GunCible" || other.gameObject.name == "ArmKnyfe" || other.gameObject.name == "ShutGun")
        {
            AmmoPlus = true;
        }
    }
}
