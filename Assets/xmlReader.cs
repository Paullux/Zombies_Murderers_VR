using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using UnityEngine.UI;

// Ce script a été rédigé par la chaîne Youtube TUTOUNITYFR, Pour savoir comment l'utiliser
// rendez-vous sur cette vidéo : https://www.youtube.com/watch?v=jVgukYyEURM

public class xmlReader : MonoBehaviour
{
	// Glissez ici votre dictionnaire XML
    public TextAsset dictionary;

    public string languageName;
    public int currentLanguage;



    // Ces variables s'adaptent en fonction du langage utilisé
    public string miseenroute;
    public string niveau;
    public string avenir;
    public string quitter;
    public string avancer;
    public string gauche;
    public string fire;
    public string nextweapon;
    public string reload;
    public string screenshot;
    public string follow;
    public string menu;
    public string nonaxe;
    public string none;
    public string pressaxis2s;
    public string pressbutton2s;
    public string decouverte;
    public string souris;
    public string creationmal;
    public string propagation;
    public string attack;
    public string armor;
    public string health;
    public string build;



    // Ces 3 variables UI ne sont pas nécessaires et peuvent être supprimées (Voir vidéo tuto)
    // MAIS il vous faudra aussi supprimer les différentes lignes relatives à ces variables

    List<Dictionary<string, string>> languages = new List<Dictionary<string, string>>();
    Dictionary<string, string> obj;

    void Awake()
    {
        Reader();
    }

    void Update()
    {
        currentLanguage = PlayerPrefs.GetInt("currentLanguage");
        //currentLanguage = 1;
        // Pour chaque phrase / mot se trouvant dans votre dictionnaire, il vous faut
        // ajouter une ligne semblable aux suivantes en modifiant la valeur se trouvant 
        // entre les " " ainsi que le "out NomDeLaVariable"
        languages[currentLanguage].TryGetValue("Name", out languageName);
        languages[currentLanguage].TryGetValue("miseenroute", out miseenroute);
        languages[currentLanguage].TryGetValue("niveau", out niveau);
        languages[currentLanguage].TryGetValue("avenir", out avenir);
        languages[currentLanguage].TryGetValue("quitter", out quitter);
        languages[currentLanguage].TryGetValue("avancer", out avancer);
        languages[currentLanguage].TryGetValue("gauche", out gauche);
        languages[currentLanguage].TryGetValue("fire", out fire);
        languages[currentLanguage].TryGetValue("nextweapon", out nextweapon);
        languages[currentLanguage].TryGetValue("reload", out reload);
        languages[currentLanguage].TryGetValue("screenshot", out screenshot);
        languages[currentLanguage].TryGetValue("follow", out follow);
        languages[currentLanguage].TryGetValue("menu", out menu);
        languages[currentLanguage].TryGetValue("nonaxe", out nonaxe);
        languages[currentLanguage].TryGetValue("none", out none);
        languages[currentLanguage].TryGetValue("pressaxis2s", out pressaxis2s);
        languages[currentLanguage].TryGetValue("pressbutton2s", out pressbutton2s);
        languages[currentLanguage].TryGetValue("decouverte", out decouverte);
        languages[currentLanguage].TryGetValue("souris", out souris);
        languages[currentLanguage].TryGetValue("creationmal", out creationmal);
        languages[currentLanguage].TryGetValue("propagation", out propagation);
        languages[currentLanguage].TryGetValue("attack", out attack);
        languages[currentLanguage].TryGetValue("armor", out armor);
        languages[currentLanguage].TryGetValue("health", out health);
        languages[currentLanguage].TryGetValue("build", out build);



    }

    void Reader()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(dictionary.text);
        XmlNodeList languageList = xmlDoc.GetElementsByTagName("language");

        foreach (XmlNode languageValue in languageList)
        {
            XmlNodeList languageContent = languageValue.ChildNodes;
            obj = new Dictionary<string, string>();

            foreach (XmlNode value in languageContent)
            {
				// Ajouter ici une condition pour chaque expression / mot de votre dictionnaire
				// n'oubliez pas de remplacer la valeur entre les " " (Attention à la casse !)
                if (value.Name == "Name")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "miseenroute")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "niveau")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "avenir")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "quitter")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "avancer")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "gauche")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "fire")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "nextweapon")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "reload")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "screenshot")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "follow")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "menu")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "nonaxe")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "none")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "pressaxis2s")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "pressbutton2s")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "decouverte")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "souris")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "creationmal")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "propagation")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "attack")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "armor")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "health")
                    obj.Add(value.Name, value.InnerText);

                if (value.Name == "build")
                    obj.Add(value.Name, value.InnerText);
            }

            languages.Add(obj);
        }
    }

}