﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertexColor : MonoBehaviour
{
    private Mesh mymesh;

    void Start()
    {
        mymesh = GetComponent<Mesh>();
        var colors32 = mymesh.colors32;
        for (int i = 0; i < colors32.Length; i++)
        {
            colors32[i] = new Color(125, 125, 125, 125);
        }
        mymesh.colors32 = colors32;
    }
}
